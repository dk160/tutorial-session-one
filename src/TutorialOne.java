import java.util.ArrayList;
import java.util.Arrays;

public class TutorialOne {

	public static Person createPerson(String name, int age) {
		Person personOne = new Person();
		personOne.setName(name);
		personOne.setAge(age);
		return personOne;
		
	}
	
	public static ArrayList<String> getNames(ArrayList<Person> people) {
		ArrayList<String> names = new ArrayList<String>();
		for(Person currentPerson : people) {
			currentPerson.setName("Mary");
			names.add(currentPerson.getName());
//			String currentName = currentPerson.getName();
//			names.add(currentName);
		}
		return names;
		
	}
	
	public static String[] getNames2(ArrayList<Person> people) {
		String[] names = new String[people.size()];
		
		for(int i = 0; i < people.size(); i++) {
			Person currentPerson = people.get(i);
			names[i] = currentPerson.getName();
		}
		return names;
		
	}
	
	public static ArrayList<Integer> getAges(ArrayList<Person> people) {
		ArrayList<Integer> ages = new ArrayList<Integer>();
		for(int i = 0; i < people.size(); i++) {
			Person currentPerson = people.get(i);
			ages.add(currentPerson.getAge());
		}
		return ages;
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Person personOne = createPerson("Sally", 17);
		Person personTwo = createPerson("Bob", 19);
		ArrayList<Person> people = new ArrayList<Person>();
		people.add(personOne);
		people.add(personTwo);
		
		String[] names = getNames2(people);
		
		ArrayList<Integer> ages = getAges(people);
		
		//System.out.println(ages);
		
		//System.out.println(Arrays.asList(names));
		
		String[] fruits = {"apple", "banana", "pineapple"};
		fruits[1] = "orange";
		for(String fruit: fruits) {
			System.out.println(fruit);
		}
		
		for(int i = 0; i < fruits.length; i++) {
			String fruit = fruits[i];
			System.out.println(fruit);
		}
		
		
		for(int i = fruits.length - 1; i >= 0; i--) {
			System.out.println(fruits[i]);
		}
		
	}
}
